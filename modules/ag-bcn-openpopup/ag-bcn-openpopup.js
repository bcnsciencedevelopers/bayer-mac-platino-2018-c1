app.register("ag-bcn-openpopup", function () {

    return {
        template: false,
        publish: {

        },
        events: {
            "swipeleft": 'noSwipe',
            "swiperight": 'noSwipe',
            "swipeup": 'noSwipe',
            "swipedown": 'noSwipe',
            "tap .js-toggle-btn": 'togglePopup',
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {

            var info = ag.platform.info();

            document.addEventListener('click', this.handleClicky.bind(this));
        },
        onRemove: function (el) {
            this._removeElement();
        },
        onEnter: function (el) {
        },
        onExit: function (el) {
            $('[data-module="ag-bcn-openpopup"]').removeClass('active');
            $('[data-module="ag-bcn-openpopup"]').html('');
        },
        noSwipe: function (e) {
            e.stopPropagation();
        },
        togglePopup: function () {
            $('.anim').toggleClass('show');
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            
            var filtro1 = el.target.dataset.filtro1;
            var filtro2 = el.target.dataset.filtro2;
            
            if( filtro1!==undefined ){
                busqueda_filtro1 = filtro1;
            }
            if( filtro2!==undefined ){
                busqueda_filtro2 = filtro2;
            }            
            
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            setTimeout(function(){
                app.goTo(go);
            }, 300);
            
        },
        handleClicky: function (event) {
            var el = event.target;
            var auxPopup = el.getAttribute('data-open-popup');
            var auxPopupProduct = el.getAttribute('data-seleccionar-producto');
            var auxPopupToggle = el.getAttribute('data-open-popup-toggle');

            if (auxPopup) {
                if (auxPopup === "close") {
                    //cierra el popup
                    $('[data-module="ag-bcn-openpopup"]').removeClass('active');
                    $('[data-module="ag-bcn-openpopup"]').html('');
                } else {
                    //abre el popup
                    currentSlideInfo = app.slideshow.resolve();

                    children = document.querySelector('#' + currentSlideInfo.slide + ' [data-popup="' + auxPopup + '"]');
                    console.log(auxPopupProduct);
                    if (!auxPopupProduct) {
                        $('[data-module="ag-bcn-openpopup"]').html('');

                        $('[data-module="ag-bcn-openpopup"]').append(
                                $('<div>').addClass('popup').addClass(currentSlideInfo.slide).addClass('abs').attr('data-popup', auxPopup).append(
                                $('<div>').addClass('close-popup').addClass('abs').attr('data-open-popup', 'close')
                                )
                                ).append(
                                $('<div>').addClass('overlay').attr('data-open-popup', 'close')
                                );
                        $('[data-module="ag-bcn-openpopup"]').addClass('active');
                    } else {
                        $('[data-module="ag-bcn-openpopup"]').html('');

                        $('[data-module="ag-bcn-openpopup"]').append(
                                $('<div>').addClass('popup').addClass(currentSlideInfo.slide).addClass('abs').attr('data-popup', auxPopup).append(
                                $('<div>').addClass('close-popup').addClass('abs').attr('data-open-popup', 'close')
                                ).append(
                                
                                $('<div>').addClass('btn-producto btn-producto-' + auxPopup + ' js-select-product').attr('data-open-popup', 'close').attr('onclick', 'seleccionarProductoNombre("'+auxPopupProduct+'")' )
                                ).append(
                                $('<div>').addClass('overlay').attr('data-open-popup', 'close')
                                ));
                        $('[data-module="ag-bcn-openpopup"]').addClass('active');
                    }

                }
            }
            if (auxPopupToggle) {
                if (auxPopupToggle === "close") {
                    //cierra el popup
                    $('[data-module="ag-bcn-openpopup"]').removeClass('active');
                    $('[data-module="ag-bcn-openpopup"]').html('');
                } else {
                    //abre el popup
                    currentSlideInfo = app.slideshow.resolve();

                    childrenToggle = document.querySelector('#' + currentSlideInfo.slide + ' [data-popup="' + auxPopupToggle + '"]');
                    if (!auxPopupProduct) {
                        $('[data-module="ag-bcn-openpopup"]').html('');

                        $('[data-module="ag-bcn-openpopup"]').append(
                                $('<div>').addClass('popup').addClass(currentSlideInfo.slide).addClass('abs').attr('data-popup', auxPopupToggle).append(
                                $('<div>').addClass('close-popup').addClass('abs').attr('data-open-popup', 'close')
                                ).append($('<div>').addClass('content-anim').append($('<div>').attr('id', auxPopupToggle + '-1').addClass('anim js-initial show')).append($('<div>').attr('id', auxPopupToggle + '-2').addClass('anim')).append($('<div>').addClass('js-toggle-btn toggle-btn')))
                                ).append(
                                $('<div>').addClass('overlay').attr('data-open-popup', 'close')
                                );
                        $('[data-module="ag-bcn-openpopup"]').addClass('active');
                    } else {
                        $('[data-module="ag-bcn-openpopup"]').html('');

                        $('[data-module="ag-bcn-openpopup"]').append(
                                $('<div>').addClass('popup').addClass(currentSlideInfo.slide).addClass('abs').attr('data-popup', auxPopupToggle).append(  
                                $('<div>').addClass('close-popup').addClass('abs').attr('data-open-popup', 'close')
                                ).append(
                                $('<div>').addClass('btn-producto btn-producto-' + auxPopupToggle + ' js-select-product2').attr('data-open-popup', 'close').attr('onclick', 'seleccionarProductoNombre("'+auxPopupProduct+'")' )).append($('<div>').addClass('content-anim').append($('<div>').attr('id', auxPopupToggle + '-1').addClass('anim js-initial show')).append($('<div>').attr('id', auxPopupToggle + '-2').addClass('anim')).append($('<div>').addClass('js-toggle-btn toggle-btn')))
                                ).append(
                                $('<div>').addClass('overlay').attr('data-open-popup', 'close')
                                );
                        $('[data-module="ag-bcn-openpopup"]').addClass('active');
                    }
                }
            }

        }
    }

});