app.register("slide_1x0", function () {

    return {
        events: {
            'swiperight #slide_2x1': 'noSwipe',
            'swipeleft #slide_2x1': 'noSwipe',
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {
            $("#master2").slider({
                value: 0,
                orientation: "horizontal",
                range: "min",
                animate: true,
                slide: function (e, ui) {
                    var val = ui.value;
                    console.log(val);
                    if (val >= 0 && val <= 25) {
                        $(".prod-1").addClass("show");
                        $(".prod-2, .prod-3").removeClass("show");
                    } else if (val >= 26 && val <= 75) {
                        $(".prod-2").addClass("show");
                        $(".prod-1, .prod-3").removeClass("show");
                    } else if (val >= 76 && val <= 100) {
                        $(".prod-3").addClass("show");
                        $(".prod-1, .prod-2").removeClass("show");
                    }
                }
            });
        },
        onRemove: function (el) {
        },
        onEnter: function (el) {
            productFamily = "Redoxon";
        },
        onExit: function (el) {
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            
            var filtro1 = el.target.dataset.filtro1;
            var filtro2 = el.target.dataset.filtro2;
            
            if( filtro1!==undefined ){
                busqueda_filtro1 = filtro1;
            }
            if( filtro2!==undefined ){
                busqueda_filtro2 = filtro2;
            }            
            
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            setTimeout(function(){
                app.goTo(go);
            }, 300);
            
        }
    };

});