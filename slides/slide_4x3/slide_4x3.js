app.register("slide_4x3", function () {
    return {
        events: {
            "tap .js-open-popup": 'openPopup',
            "tap .js-close-popup": 'closePopup',
            swipeleft: pararEventos,
            swiperight: pararEventos,
            'swipeup #item4x2_1': 'noSwipe',
            'swipedown #item4x2_1': 'noSwipe',
            'tap .js-reset': resetDraggable,
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {
            $("#item4x3_1").draggable({
                revert: "invalid",
                drag: function (e) {
                    $(this).removeClass('clickme');
                }
            });
            $("#item4x3_1").data({'originalLeft': $("#item4x3_1").css('left'), 'origionalTop': $("#item4x3_1").css('top')});

            $("#zonadrop4x3_1").droppable({
                accept: "#item4x3_1",
                drop: function (event, ui) {
                    $("#item4x3_1").css('top', '33.5%').css('left', '67.1%').animate({opacity: 0}, 800);

                    $("#contenido .contenido-1").animate({opacity: 0}, 1000);
                    $("#contenido .contenido-2").animate({opacity: 1}, 1000);
                    $('.js-reset').removeClass('hidden');
                    $('.flecha-contenido').animate({opacity: 0}, 500);
                }
            });

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

        },
        onExit: function (el) {
            resetDraggable();
        },
        openPopup: function (ele) {
            var pop = ele.target.dataset.popup;
            $(pop).addClass('active');
            $(".overlay").fadeIn(200);
            $(".logo").fadeOut(200);
        },
        closePopup: function () {
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        noSwipe: function (e) {
            e.stopPropagation();
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            
            var filtro1 = el.target.dataset.filtro1;
            var filtro2 = el.target.dataset.filtro2;
            
            if( filtro1!==undefined ){
                busqueda_filtro1 = filtro1;
            }
            if( filtro2!==undefined ){
                busqueda_filtro2 = filtro2;
            }            
            
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            setTimeout(function(){
                app.goTo(go);
            }, 300);
            
        }
    };

    function resetDraggable() {
        $("#item4x3_1").css({'left': $("#item4x3_1").data('originalLeft'),
            'top': $("#item4x3_1").data('origionalTop'),
            'opacity': 1}).addClass('clickme');

        $("#contenido .contenido-1").animate({opacity: 1}, 500);
        $("#contenido .contenido-2").animate({opacity: 0}, 500);
        $('.js-reset').addClass('hidden');
        $('.flecha-contenido').animate({opacity: 1}, 500);
    }

});