app.register("slide_4x2", function () {
    var val = 0;

    return {
        events: {
            "tap .js-open-popup": 'openPopup',
            "tap .js-close-popup": 'closePopup',
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {
            $("#master").slider({
                orientation: "horizontal",
                range: "min",
                min: 0,
                max: 1,
                value: 0,
                animate: true,
                slide: function (e, ui) {
                    val = ui.value;
                    console.log(val);
                    if (val === 0) {
                        $(".content1").fadeIn();
                        $(".content2").fadeOut();
                    } else {
                        $(".content2").fadeIn();
                        $(".content1").fadeOut();;
                    }
                }
            });

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Respisaline";
            document.removeEventListener('swipeleft', app.slideshow.right);
            document.removeEventListener('swiperight', app.slideshow.left);

            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        onExit: function (el) {
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        openPopup: function (ele) {
            var pop = ele.target.dataset.popup;
            $(pop).addClass('active');
            $(".overlay").fadeIn(200);
            $(".logo").fadeOut(200);
        },
        closePopup: function () {
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        selectProduct: function (el) {
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        }
    }

});
