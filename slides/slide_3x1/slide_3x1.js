app.register("slide_3x1", function () {
    return {
        events: {
            'swiperight #slide_3x1': 'noSwipe',
            'swipeleft #slide_3x1': 'noSwipe',
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {
        },
        onRemove: function (el) {
        },
        onEnter: function (el) {
            productFamily = "Redoxon";
        },
        onExit: function (el) {
        },
        noSwipe: function (e) {
            e.stopPropagation();
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            
            var filtro1 = el.target.dataset.filtro1;
            var filtro2 = el.target.dataset.filtro2;
            
            if( filtro1!==undefined ){
                busqueda_filtro1 = filtro1;
            }
            if( filtro2!==undefined ){
                busqueda_filtro2 = filtro2;
            }            
            
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            setTimeout(function(){
                app.goTo(go);
            }, 300);
            
        }
    }

});