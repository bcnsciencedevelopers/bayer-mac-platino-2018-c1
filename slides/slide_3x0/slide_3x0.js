app.register("slide_3x0", function () {

    return {
        events: {
            swipeup: pararEventos,
            "tap .js-open-popup": 'openPopup',
            "tap .js-close-popup": 'closePopup',
            "tap .js-toggle-btn": 'togglePopup',
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {

            $("#master2").slider({
                value: 0,
                orientation: "horizontal",
                range: "min",
                animate: true,
                slide: function (e, ui) {
                    var val = ui.value;
                    console.log(val);
                    if (val >= 0 && val <= 25) {
                        $(".prod-1").addClass("show");
                        $(".prod-2, .prod-3").removeClass("show");
                    } else if (val >= 26 && val <= 75) {
                        $(".prod-2").addClass("show");
                        $(".prod-1, .prod-3").removeClass("show");
                    } else if (val >= 76 && val <= 100) {
                        $(".prod-3").addClass("show");
                        $(".prod-1, .prod-2").removeClass("show");
                    }
                }
            });
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            document.removeEventListener('swipeleft', app.slideshow.right);
            document.removeEventListener('swiperight', app.slideshow.left);
            
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        onExit: function (el) {
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        openPopup: function (ele) {
            var pop = ele.target.dataset.popup;
            $(pop).addClass('active');
            $(".overlay").fadeIn(200);
            $(".logo").fadeOut(200);
        },
        closePopup: function () {
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
            setTimeout(function () {
                $('.anim').removeClass('show');
                $('.js-initial').addClass('show');
            }, 200);
        },
        togglePopup: function () {
            $('.anim').toggleClass('show');
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            
            var filtro1 = el.target.dataset.filtro1;
            var filtro2 = el.target.dataset.filtro2;
            
            if( filtro1!==undefined ){
                busqueda_filtro1 = filtro1;
            }
            if( filtro2!==undefined ){
                busqueda_filtro2 = filtro2;
            }            
            
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            setTimeout(function(){
                app.goTo(go);
            }, 300);
            
        }
    };

});