//variables


// Declarar variables GLOBALES
var busqueda_filtro0 = "0";
var busqueda_filtro1 = "0";
var busqueda_filtro2 = "0";

var listado;
var isChrome = !!window.chrome && !!window.chrome.webstore;
// Ruta xml
var resources2 = "resources/";
var material_select = "slides/expositor/assets/xml/filtros.xml";
var material = "slides/expositor/assets/xml/materiales.xml";

var miBusqueda = [];
var swiperActiveIndex = 5;

app.register("expositor", function () {
    var render;

    return {
        states: [],
        events: {
            "tap .js-abrir-lista": 'abrirLista',
            "tap .js-abrir-lista > li": 'seleccionarFiltro',
            swipedown: 'noSwipe',
            swipeup: 'noSwipe',
        },
        onRender: function (el) {
            // Llamar funcion para cargar los datos en el select del material cap.
            getMaterialFromXML(material_select); //leer el xml
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            document.removeEventListener('swipeleft', app.slideshow.right);
            document.removeEventListener('swiperight', app.slideshow.left);
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);

            // LLamar funcion para mostrar todos el listado - No repetirla en todo el codigo por evitar errores de duplicado de productos.
            getAllListFromXML(material);

        },
        onExit: function (el) {
//            busqueda_filtro0 = "0";
//            busqueda_filtro1 = "0";
//            busqueda_filtro2 = "0";
        },
        abrirLista: function (ele) {
            listado = "#" + ele.target.id;
            if (listado != '#') {
                $(listado + " li").slideToggle();
                $(listado + "> div").toggleClass("rotar");
            }
        },
        seleccionarFiltro: function (ele) {
            var padre = "#" + ele.target.parentElement.id;
            if (padre != '#') {
                $(padre + "> div").toggleClass("rotar");
            }
            var content = ele.target.innerHTML;
            var value = ele.target.dataset.value;

            $(padre + " li").slideToggle();
            $(padre + " span").html(content);
            mostrarSelect(padre, value);
        },
        noSwipe: function (e) {
            e.stopPropagation();
        },
    }

});






/**********************************************
 *                                             *
 *               FUNCIONES EXTRAS              *
 *                                             *
 ***********************************************/


/*
 * Carga todos los selects del xml.
 */
function getMaterialFromXML(ruta_xml) {

    // Cogemos el xml..
    $.get(ruta_xml, function (xml) {
        createSelect(xml, "marca", false);
        createSelect(xml, "producto", false, busqueda_filtro1);
        createSelect(xml, "categoria", false);
    });// Get
}

/*
 * Monta el select
 */
function createSelect(xml, campo, filtroMac, selected) {

    selected = (selected && selected !== "0") ? selected : campo;
    console.log(selected);
    $("#lista_" + campo).html('');
    $("#lista_" + campo).append('<span>' + selected + '</span><div></div>');

    $(xml).find("filtros").each(function (key, value) {
        $(this).find(campo).each(function (key, value) {
            $(this).find('item').each(function (num, value) {
                if (campo === "marca") {
                    $("#lista_" + campo).append('<li data-value="' + $(this).find('value').text() + '">' + $(this).find('name').text() + '</li>');
                } else {
                    var marca = $(this).find('marca').text();
                    if (marca == filtroMac || !filtroMac) {
                        $("#lista_" + campo).append('<li data-value="' + $(this).find('value').text() + '">' + $(this).find('name').text() + '</li>');
                    }
                }
            });
        });
        $("#lista_" + campo).append('<li data-value="0">Mostrar todo</li>');
    });// Each
}

/*
 * Coger todos los objetos del XML en un array.
 */
function getAllListFromXML(xml) {

    miBusqueda = [];

    // Cogemos el xml..
    $.get(xml, function (xml) {

        // Buscamos el objeto 'selects'
        $(xml).find("materiales").each(function () {

            // Buscamos los elementos padre dentro del selects
            $(this).find('material').each(function (index, value) {

                // Elementos especifico del xml
                var id = $(value).find('id').text();
                var nombre = $(value).find('nombre').text();
                var filtro0 = $(value).find('filtro0').text();
                var filtro1 = $(value).find('filtro1').text();
                var filtro2 = $(value).find('filtro2').text();
                var img = $(value).find('img').text();
                var descripcion = $(value).find('descripcion').text();
                var codigo = $(value).find('codigo').text();
                var medidas = $(value).find('medidas').text();
                var pvl = $(value).find('pvl').text();
                var logo = $(value).find('logo').text();
                var baldas = $(value).find('baldas').find('balda');
                var balda1 = $(baldas[0]).text();
                var balda2 = $(baldas[1]).text();
                var tipo = $(value).find('tipo').text();
                var informacion = $(value).find('informacion').text();

                // añadir el resto de valores
                //miBusqueda.push(setJSON(id, nombre, filtro0, filtro1, filtro2, img, descripcion, codigo, medidas, pvl, logo, balda1, balda2));
                if ((busqueda_filtro0 == "0" && busqueda_filtro1 == "0" && busqueda_filtro2 == "0") ||
                        (busqueda_filtro0 == filtro0 && busqueda_filtro1 == "0" && busqueda_filtro2 == "0") ||
                        (busqueda_filtro0 == "0" && busqueda_filtro1 == filtro1 && busqueda_filtro2 == "0") ||
                        (busqueda_filtro0 == "0" && busqueda_filtro1 == "0" && busqueda_filtro2 == filtro2) ||
                        (busqueda_filtro0 == filtro0 && busqueda_filtro1 == filtro1 && busqueda_filtro2 == "0") ||
                        (busqueda_filtro0 == "0" && busqueda_filtro1 == filtro1 && busqueda_filtro2 == filtro2) ||
                        (busqueda_filtro0 == filtro0 && busqueda_filtro1 == "0" && busqueda_filtro2 == filtro2) ||
                        (busqueda_filtro0 == filtro0 && busqueda_filtro1 == filtro1 && busqueda_filtro2 == filtro2)) {
                    miBusqueda.push(setJSON(id, nombre, filtro0, filtro1, filtro2, img, descripcion, codigo, medidas, pvl, logo, balda1, balda2, tipo, informacion));
                }
                //if de filtro2 miras si tiene una coma, si tiene una coma haces un split de la coma y miras los dos resultados de la array, si coincide uno haces el push
                else if (filtro2.indexOf('|') > -1) {
                    var aux = filtro2.split('|');
                    var arrayLength = aux.length;
                    for (var i = 0; i < arrayLength; i++) {
                        if (busqueda_filtro2 == aux[i]) {
                            miBusqueda.push(setJSON(id, nombre, filtro0, filtro1, filtro2, img, descripcion, codigo, medidas, pvl, logo, balda1, balda2, tipo, informacion));
                            break;
                        }
                    }
                }
            }
            );// Each
        });// Each
        createDiv();

    });// Get
}

/**
 * Convertir en un array el xml.
 */
function setJSON(id, nombre, filtro0, filtro1, filtro2, img, descripcion, codigo, medidas, pvl, logo, balda1, balda2, tipo, informacion) {

    return {
        'id': id,
        'nombre': nombre,
        'filtro0': filtro0,
        'filtro1': filtro1,
        'filtro2': filtro2,
        'img': img,
        'descripcion': descripcion,
        'codigo': codigo,
        'medidas': medidas,
        'pvl': pvl,
        'logo': logo,
        'balda1': balda1,
        'balda2': balda2,
        'tipo': tipo,
        'informacion': informacion
    };
}

/*
 * Crea el div de la celda.
 */
function createDiv() {
    var miDiv = '';
    var div_list_view_celdas = $("#div_list_view");

    div_list_view_celdas.html('');
    var contTotal = 0;
    var color;
    miDiv += '<div class="swiper-container" id="swiper-container">';
    miDiv += '<div class="swiper-wrapper">';
    $.each(miBusqueda, function (index, value) {
        if (value.filtro0.search("Azul") > -1) {
            value['color'] = "azul";
        } else {
            value['color'] = "verde";
        }
        miDiv += print(value, index);
        contTotal++;
    });
    miDiv += '</div>';
    miDiv += '</div>';
    miDiv += '<div class="swiper-button-next"></div>';
    miDiv += '<div class="swiper-button-prev"></div>';

    if (contTotal == 0) {
        miDiv = '<h2 class="SinResultado">No hay ningún resultado</h2>';
    }

    // Insertamos la celda al div del html.
    div_list_view_celdas.append(miDiv);

    var swiper = new Swiper('#swiper-container', {
        paginationClickable: true,
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        initialSlide: swiperActiveIndex,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        coverflow: {
            rotate: 0,
            stretch: 50,
            depth: 588,
            modifier: 1,
            slideShadows: false
        },
        onSlideChangeEnd: function (swiper) {
            swiperActiveIndex = swiper.activeIndex;
        }
    });
}

/*
 * Printar el div de la celda.
 */
function print(value, index) {
    return ('<div class="swiper-slide">' +
            '<div class="contenedor ' + value.color + '">' +
            '<div class="div_img_diapo" onclick="seleccionarProducto(\'' + index + '\')">' +
            '<img src="slides/expositor/assets/img/' + value.img + '" class="img_diapo" />' +
            '<div class="btn_pop"></div>' +
            '</div>' +
            '<div class="div_txt">' +
            '<p class="text_nombre ">' + value.nombre + '</p>' +
            '</div>' +
            '</div>' +
            '</div>'
            );
}


/**********************************************
 *                                             *
 *               FUNCIONES EVENTOS             *
 *                                             *
 ***********************************************/

/*
 * Filtro de los selects.
 */
function mostrarSelect(id, filtro) {

    if ($(id).attr('id') == 'lista_marca') {

        if (filtro == "0") {
            busqueda_filtro0 = filtro;
            filtro = false;
        } else {
            if (busqueda_filtro0 !== filtro) {
                busqueda_filtro1 = false;
                busqueda_filtro2 = false;
            }
            busqueda_filtro0 = filtro;
        }

        $.get(material_select, function (xml) {
            createSelect(xml, "producto", filtro, busqueda_filtro1);
            createSelect(xml, "categoria", filtro, busqueda_filtro2);
        });

    }
    if ($(id).attr('id') === 'lista_producto') {
        busqueda_filtro1 = filtro;
    }
    if ($(id).attr('id') === 'lista_categoria') {
        busqueda_filtro2 = filtro;
    }
    getAllListFromXML(material);
}

function seleccionarProducto(index) {
    var elemento = miBusqueda[index];

    renderProducto.carpeta = elemento.id;
    renderProducto.color = elemento.color;
    renderProducto.descripcion = elemento.descripcion;
    renderProducto.codigo = elemento.codigo;
    renderProducto.medidas = elemento.medidas;
    renderProducto.img = elemento.img;
    renderProducto.pvl = elemento.pvl;
    renderProducto.nombre = elemento.nombre;
    renderProducto.filtro0 = elemento.filtro0;
    renderProducto.filtro1 = elemento.filtro1;
    renderProducto.logo = elemento.logo;
    renderProducto.balda1 = elemento.balda1;
    renderProducto.balda2 = elemento.balda2;
    renderProducto.tipo = elemento.tipo;
    renderProducto.informacion = elemento.informacion;
    app.goTo('test_collection_1/producto');

}


/*
 * Filtro de los selects desde el boton del producto directo
 */
function seleccionarProductoNombre(nombre) {
    console.log(nombre);

    console.log(material);

    $.get(material, function (xml) {

        // Buscamos el objeto 'selects'
        $(xml).find("material").each(function () {

            if ($(this).find('id').text() == nombre) {
                var producto = Array();
                var id = $(this).find('id').text();
                var filtro0 = $(this).find('filtro0').text();
                var filtro1 = $(this).find('filtro1').text();
                var filtro2 = $(this).find('filtro2').text();
                var img = $(this).find('img').text();
                var descripcion = $(this).find('descripcion').text();
                var codigo = $(this).find('codigo').text();
                var medidas = $(this).find('medidas').text();
                var pvl = $(this).find('pvl').text();
                var logo = $(this).find('logo').text();
                var baldas = $(this).find('baldas').find('balda');
                var balda1 = $(baldas[0]).text();
                var balda2 = $(baldas[1]).text();
                var tipo = $(this).find('tipo').text();
                var informacion = $(this).find('informacion').text();

                producto.push(setJSON(id, nombre, filtro0, filtro1, filtro2, img, descripcion, codigo, medidas, pvl, logo, balda1, balda2, tipo, informacion));
                mostrarProducto(producto);
                return true;
            }
        });
    });
}

/*
 * Vinculo para montar en "producto" los datos del XML del producto seleccionado.
 */
function mostrarProducto(producto) {

    producto = producto[0];
    var color = producto.filtro0;
    if (color.search("Azul") > -1) {
        color = "azul";
    } else {
        color = 'verde';
    }
    renderProducto.carpeta = producto.id;
    renderProducto.color = color;
    renderProducto.descripcion = producto.descripcion;
    renderProducto.codigo = producto.codigo;
    renderProducto.medidas = producto.medidas;
    renderProducto.img = producto.img;
    renderProducto.pvl = producto.pvl;
    renderProducto.nombre = producto.nombre;
    renderProducto.filtro0 = producto.filtro0;
    renderProducto.filtro1 = producto.filtro1;
    renderProducto.logo = producto.logo;
    renderProducto.balda1 = producto.balda1;
    renderProducto.balda2 = producto.balda2;
    renderProducto.tipo = producto.tipo;
    renderProducto.informacion = producto.informacion;
    app.goTo('test_collection_1/producto');
}