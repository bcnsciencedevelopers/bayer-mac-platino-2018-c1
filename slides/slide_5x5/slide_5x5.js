app.register("slide_4x5", function () {

    return {
        events: {
//            swipeup: pararEventos,
//            swipedown: pararEventos,
            "tap .js-open-popup" : 'openPopup',
            "tap .js-close-popup" : 'closePopup',
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            document.removeEventListener('swipeleft', app.slideshow.right);
            document.removeEventListener('swiperight', app.slideshow.left);
            
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        onExit: function (el) {
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        openPopup: function( ele ){
            var pop = ele.target.dataset.popup;
            $(pop).addClass('active');
            $(".overlay").fadeIn(200);
            $(".logo").fadeOut(200);
        },
        closePopup: function(){
            $('.popup').removeClass('active');
            
            $(".logo").fadeIn(200);
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            var filtro2 = el.target.dataset.filtro2;
            busqueda_filtro1 = filtro1;
            busqueda_filtro2 = filtro2;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        }
    }

});
