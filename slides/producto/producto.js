

//window.onload = initKeyShotVR('ginecanesgel');

app.register("producto", function () {
    return {
        events: {
            swipedown: 'noSwipe',
            swipeup: 'noSwipe',
            'tap .mas-pvl': togglePvl,
            'tap .js-show-balda-1': 'mostrarBalda1',
            'tap .js-show-balda-2': 'mostrarBalda2',
            'tap .js-show-producto': 'mostrarProducto',
            "tap .jumpToLastSlideProduct": "jumpToLastSlideProduct"
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            
            document.removeEventListener('swipeleft', app.slideshow.right);
            document.removeEventListener('swiperight', app.slideshow.left);
            
            initKeyShotVR(renderProducto.carpeta);
            
            $('#render-baldas').html("");
            $("#producto").addClass(renderProducto.color);
            $(".producto-id").html(renderProducto.carpeta);
            $(".producto-logo").html("<img src='slides/expositor/assets/xml/img/" + renderProducto.logo + "' alt='' />");
            $(".producto-nombre").html(renderProducto.filtro0 + " · " + renderProducto.nombre + "®");
            $(".producto-descripcion").html(renderProducto.descripcion);
            $("#producto-pvl").html(renderProducto.pvl);
            $(".wrapper-baldas").html("");

            if (renderProducto.codigo)
                $(".producto-codigo").html("C.N.: " + renderProducto.codigo);
            if (renderProducto.medidas)
                $(".producto-medidas").html("Medidas: " + renderProducto.medidas);
            if (renderProducto.balda1 !== "")
                $(".wrapper-baldas").append("<img class='js-show-balda-1' src='slides/producto/assets/baldas/" + renderProducto.balda1 + "' alt='' />");
            if (renderProducto.balda2 !== "")
                $(".wrapper-baldas").append("<img class='js-show-balda-2' src='slides/producto/assets/baldas/" + renderProducto.balda2 + "' alt='' />");
            if (renderProducto.balda1 !== "" || renderProducto.balda2 !== "")
                $(".wrapper-baldas").append("<img class='js-show-producto active' src='slides/expositor/assets/img/" + renderProducto.img + "' alt='' />");
            if(renderProducto.filtro1 == "Clarityne"){
                $("#viewwindow").remove();
                $('#render-producto').append("<img class='' src='slides/producto/clarityne/0_0.png' alt='' />");
            } 
            
            
            if(renderProducto.informacion == ""){
                $(".producto-informacion").hide();
            }else{
                $(".producto-informacion").show();
            }
            
//            if (renderProducto.carpeta == 'iberogast' || renderProducto.carpeta == 'ginecanesflorexpo' || renderProducto.carpeta == 'bepantholcalmcremaexpo' || renderProducto.carpeta == 'bepantholsensidaily') {
//                $(".producto-pvl").addClass('big');
//            } else {
//                $(".producto-pvl").removeClass('big');
//            }
            
            var pvlBr = renderProducto.pvl;
            var cont = (pvlBr.match(/<\/br>/g) || []).length;
           
            $("#producto-pvl").addClass("bocadillo-pvl-" + cont + "");
            



//            if (renderProducto.carpeta == 'supradynmagpo') {
//                $(".producto-pvl").addClass('sbig');
//            } else {
//                $(".producto-pvl").removeClass('sbig');
//            }
        },
        onExit: function (el) {
            $("#producto").removeClass(renderProducto.color);
            $("#render-baldas").remove();
            $("#render-producto").html("");
            $(".bocadillo-pvl").removeClass("toggle-pvl");
            $("#producto-pvl").removeClass();
            $(".wrapper-baldas").html("");
            $(".producto-logo").html("");
            $(".producto-descripcion").html("");
            $(".producto-informacion").html("");
            $(".producto-codigo").html("");
            $(".producto-medidas").html("");
            $("#producto-pvl").html("");
            $(".bocadillo-pvl").html("");
        },
        noSwipe: function (e) {
            e.stopPropagation();
        },
        mostrarBalda1: function(e){
            $('.js-show-balda-1').addClass('active');
            $('.js-show-producto, .js-show-balda-2').removeClass('active');
            $('#render-producto').hide();
            $('#producto').append("<div id='render-baldas' style='display:block'><img class='js-show-balda' src='slides/producto/assets/baldas/" + renderProducto.balda1 + "' /></div>");
        },
        mostrarBalda2: function(e){
            $('.js-show-balda-2').addClass('active');
            $('.js-show-producto, .js-show-balda-1').removeClass('active');
            $('#render-producto').hide();
            $('#render-baldas').html("<img class='js-show-balda' src='slides/producto/assets/baldas/" + renderProducto.balda2 + "' />").show();
        },
        mostrarProducto: function(e){
            $('.js-show-balda-1, .js-show-balda-2').removeClass('active');
            $('.js-show-producto').addClass('active');
            $('#render-producto').show();
            $('#render-baldas').remove();
        },
        jumpToLastSlideProduct: function (e) {
            app.$.BackNavigation.back();
            app.$.menu.updateCurrent();
            app.$.toolbar.hide();
        }
    };
});

function togglePvl() {
    $("#producto-pvl").toggleClass("toggle-pvl");
}